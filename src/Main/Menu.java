package Main;

import Opciones.Cliente;
import Opciones.PasarelaDePago;
import Opciones.Pedido;
import Opciones.Producto;

import java.util.Date;
import java.util.Scanner;
                //bucle del menu//
/**
 *<p>Es un bucle que pide por pantalla la opcion
 * y hay que introducir que opcion quieres</p>
 * Contiene un while
 *
 */
/**
 * <h1>Esto es el menu</h1>
 * <p>
 *     Esta es una parte de mi practica
 *     que contiene el main.
 * </p>
 * @author Angel mayancela saeteros
 * @version 1.00
 * @since 09/02/2022
 */
  public class Menu {
     public static void main(String[] args) {
        boolean salir=false;
        while (!salir){
        System.out.println();
        System.out.println(" BIENVENIDO A LA GESTION DE PEDIDOS:");
        System.out.println();
            System.out.println("Cliente--(1)");
            System.out.println("Producto--(2)");
            System.out.println("Pedido--(3)");
            System.out.println("Pagos--(4)");
        System.out.println("Introduce la opcion:");

        Scanner sc = new Scanner(System.in);
        int opcion =  sc.nextInt();

            switch (opcion) {
                case 1:
                    System.out.println("HAS SELECCIONADO LA OPCION DE CLIENTE");
                    System.out.println();
                    Cliente datoscli = new Cliente();
                    opcioncliente(datoscli);
                    datoscli.verdatos();

                    break;
                case 2:
                    System.out.println("HAS SELECCIONADO LA OPCION DE PRODUCTO");
                    System.out.println();
                    Producto pro1 = new Producto();
                    Producto pro2 = new Producto();
                    Scanner scpro = new Scanner(System.in);
                    System.out.println("Nombre del primer producto:");
                    pro1.setNombre((scpro.next()));
                    System.out.println("Precio:");
                    pro1.setPrecio((scpro.nextInt()));
                    System.out.println("Cantidad:");
                    pro1.setCantida((scpro.nextInt()));

                    System.out.println("Nombre del segundo producto:");
                    pro2.setNombre((scpro.next()));
                    System.out.println("Precio:");
                    pro2.setPrecio((scpro.nextInt()));
                    System.out.println("Cantidad:");
                    pro2.setCantida((scpro.nextInt()));
                    pro1.mostrarproducto(pro1);
                    pro2.mostrarproducto(pro2);
                    break;
                case 3:
                    System.out.println("HAS SELECCIONADO LA OPCION DE PEDIDO");
                    System.out.println();
                    Pedido datospedido1 = new Pedido();
                    opcionpedido(datospedido1);
                    datospedido1.verpedido();
                    System.out.println();
                    break;

                case 4:
                    System.out.println("HAS SELECCIONADO LA OPCION DE PAGOS");
                    System.out.println();
                    PasarelaDePago pago1 = new PasarelaDePago();
                    opcionpago(pago1);
                    break;

                case 5:
                    salir = true;
                    break;
             }
        }
    }
       //metodos

    /** opcion del cliente
     * Pide por pantalla los datos del cliente
     * @param cliente
     * @return nombre=nombre,
     */
        public static  void opcioncliente(Cliente cliente) {
        System.out.println("Introducir el cliente:");
                System.out.println();
                Scanner sc = new Scanner(System.in);
                System.out.println("Introduce el Nombre:");
                cliente.setNombre(sc.next());
                System.out.println("Introduce el Apellido:");
                cliente.setApellido(sc.next());
                System.out.println("Introduce el fecha de alta:");
                cliente.setFechadealta(new Date());
                System.out.println("Introduce el telefono:");
                cliente.setTelefono(sc.nextInt());
                System.out.println("Introduce el movil:");
                cliente.setMovil(sc.nextInt());
                System.out.println("Introduce el Historial:");
                cliente.setHistorial(sc.next());
                System.out.println("Introduce el direccion:");
                cliente.setDireccion(sc.next());

        }

        /**Pasarela de pago
         * Elige la opcion de pago
         * @param pasarelaDePago
         * @return pasareladepago=pasareladepago
         */
        public static void opcionpago(PasarelaDePago pasarelaDePago) {
            System.out.println("Introduce la opcion de pago:");
            System.out.println("Tarjeta--(2)");
            System.out.println("Efectivo--(1)");
            System.out.println("Cuenta--(3)");
            Scanner sc = new Scanner(System.in);
            int pago = sc.nextInt();
            switch (pago) {
                case 1:
                    System.out.println("Pagado en Efectivo");
                    System.out.println("Pagado");
                    break;
                case 2:
                    System.out.println("Pagado en Tarjeta");
                    System.out.println("Aceptado");
                    break;
                case 3:
                    System.out.println("Pagado en Cuenta");
                    System.out.println("Verficado");
                    break;
            }
        }

            /**
         * Opcion de pedido
         * pide por pantalla lo quieres en la opcion del pedido
         * @param pedido
         * @return pedido=pedido
         */
        public static void opcionpedido(Pedido pedido) {
            System.out.println("Introducir los datos de pedido---(1)");
            System.out.println("Introducir pedidos---(2)");
            System.out.println("Eliminar producto---(3)");
            System.out.println();
            System.out.println("Introduce la opcion de pedido:");
            Scanner sca = new Scanner(System.in);
            int pedi = sca.nextInt();
            switch (pedi) {
                case 1:
                    Scanner scp = new Scanner(System.in);
                    System.out.println("Introduce el Cliente:");
                    pedido.setCliente(scp.next());
                    System.out.println("Introduce el Producto 1:");
                    pedido.setProducto1(scp.next());
                    System.out.println("Introduce el Producto 2:");
                    pedido.setProducto2(scp.next());
                    System.out.println("Introduce el Importe total:");
                    pedido.setImportetotal(scp.nextInt());
                    System.out.println("Introduce el Pago:");
                    pedido.setPago(scp.next());
                    System.out.println("Introduce el Estado:");
                    pedido.setEstado(scp.next());
                    break;
                case 2:
                    Scanner scint = new Scanner(System.in);
                    System.out.println("Introduce el producto que quieres: ");
                    scint.next();
                    System.out.println("El producto a sido agredado gracias!!");

                    break;
                case 3:
                    Scanner scelim = new Scanner(System.in);
                    System.out.println("Introduce el producto que quieres eliminar: ");
                    scelim.next();
                    System.out.println("El producto a sido eliminado!");
                    break;
            }
        }
    }
