package Opciones;
/**
 * <h1>Producto</h1>
 * <p>
 *     Esta es una parte del producto contienen
 *     las variables y la opcion de enseñarlo
 * </p>
 * @author Angel mayancela saeteros
 * @version 1.0
 * @since 09/02/2022
 */
public class Producto {
    //atributo
    private String nombre;
    /**
     * El nombre del producto
     * @var string
     */

    private int precio;
    /**
     * El precio
     * @var int
     */
    private int cantidad;
    /**
     *la cantidad
     * @var int
     */

    //constructor vacio
        public Producto(){

        }
// Constructor
    /**
     * Constructor
     * @param nombre
     * @param precio
     * @param cantidad
     */
    public Producto(String nombre, int precio, int cantidad) {
        this.nombre = nombre;
        this.precio = precio;
        this.cantidad = cantidad;
    }
    // Get y Set

    public String getNombre() {
        return nombre;
    }
    /**
     * Función para devolver el nombre
     * @param
     */

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    /**
     * Función para introduce el nombre
     * @return nombre,
     */

    public int getPrecio() {
        return precio;
    }
    /**
     * Función para devolver el precio
     * @return precio,
     */

    public void setPrecio(int precio) {
        this.precio = precio;
    }
    /**
     * Función para introduce el precio
     * @return precio,
     */

    public int getCantidad() {
        return cantidad;
    }
    /**
     * Función para devolver el cantidad
     * @return csntidad,
     */

    public void setCantida(int cantidad) {
        this.cantidad = cantidad;
    }
    /**
     * Función para introduce el cantidad
     * @return cantidad,
     */
    // muestra el producto

    /**rellena el nombre ,precio,cantidad
     * @param producto
     * @return nombre=nombre,precio=precio,cantidad=cantidad
     */
    public void mostrarproducto(Producto producto){
        System.out.println("***Productos***");
        System.out.println();
        System.out.println("Nombre:"+" "+this.nombre);
        System.out.println("Precio:"+" "+this.precio);
        System.out.println("Cantidad:"+" "+this.cantidad);
    }

}

