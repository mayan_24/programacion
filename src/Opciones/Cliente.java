package Opciones;

import java.util.Date;
import java.util.Locale;
import java.util.Scanner;
/**
 * <h1>Cliente</h1>
 * <p>
 *     Esta es una parte del cliente contienen
 *     las variables y una opcion de mostrar el cliente
 * </p>
 * @author Angel mayancela saeteros
 * @version 1.0
 * @since 09/02/2022
 */
public class Cliente {
    //Atributos
        private String nombre;
    /**
     * El nombre del cliente
     * @var string
     */
        private String apellido;
    /**
     * El apellido del cliente
     * @var string
     */
        private Date fechadealta;
    /**
     * la fecha de alta del cliente
     * @var Date
     */
        private int telefono;
    /**
     * El telefono del cliente
     * @var int
     */
        private int movil;
    /**
     * El movil del cliente
     * @var int
     */
        private String historial;
    /**
     * El historial del cliente
     * @var string
     */
        private String direccion;
    /**
     * La direccion del cliente
     * @var string
     */
        private String pedido;
    /**
     * El pedido del cliente
     * @var string
     */

    //metodo vacio
        public  Cliente(){

        }
    // Constructor
    /**
     * Constructor
     * @param nombre
     * @param apellido
     * @param fechaDeAlta
     * @param telefono
     * @param movil
     * @param historial
     * @param direccion
     */
        public Cliente(String nombre, String apellido, Date fechaDeAlta, int telefono, int movil, String historial, String direccion) {
            this.nombre = nombre.toLowerCase();
            this.apellido = apellido.toUpperCase();
            this.fechadealta = (fechaDeAlta=new Date());
            this.telefono = telefono;
            this.movil = movil;
            this.historial = historial;
            this.direccion = direccion;
        }

    public String getNombre() {
        return nombre;
    }
    /**
     * Función para devolver el nombre
     * @return nombre,
     */

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    /**
     * Función para introduce el nombre
     * @return nombre,
     */

    public String getApellido() {
        return apellido;
    }
    /**
     * Función para devolver el apellido
     * @return apellido,
     */

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }
    /**
     * Función para introduce el apellido
     * @return apellido,
     */

    public Date getFechadealta() {
        return fechadealta;
    }
    /**
     * Función para devolver la fecha de alta
     * @return fechadealta,
     */

    public void setFechadealta(Date fechadealta) {
        this.fechadealta = fechadealta;
    }
    /**
     * Función para introduce la fecha de alta
     * @return fechadealta,
     */

    public int getTelefono() {
        return telefono;
    }
    /**
     * Función para devolver el telefono
     * @return telefono,
     */

    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }
    /**
     * Función para introduce el telefono
     * @return telefono,
     */
    public int getMovil() {
        return movil;
    }
    /**
     * Función para devolver el movil
     * @return movil,
     */

    public void setMovil(int movil) {
        this.movil = movil;
    }
    /**
     * Función para introduce el movil
     * @return movil,
     */

    public String getHistorial() {
        return historial;
    }
    /**
     * Función para devolver el historial
     * @return historial,
     */

    public void setHistorial(String historial) {
        this.historial = historial;
    }
    /**
     * Función para introduce el historial
     * @return historial,
     */

    public String getDireccion() {
        return direccion;
    }
    /**
     * Función para devolver la direccion
     * @return direccion,
     */

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }
    /**
     * Función para introduce la direccion
     * @return direccion,
     */

    public String getPedido() {
        return pedido;
    }
    /**
     * Función para devolver el pedido
     * @return pedido,
     */

    public void setPedido(String pedido) {
        this.pedido = pedido;
    }
    /**
     * Función para introduce el pedido
     * @return pedido,
     */


    /**muestra los datos del cliente en el main
     * @param
     * @return nombre=nombre,apoellido=apellido,fechadealta=fechadealta,telefono=telefono,movil=movil,hitorial=historial,direccion=direccion
     */
    public void verdatos(){
            System.out.println("***Clientes***");
            System.out.println();
            System.out.println("Nombre:"+" " +this.nombre.toLowerCase());
            System.out.println("Apellido:"+" "+ this.apellido.toUpperCase());
            System.out.println("FechaDeAlta:"+" "+ (fechadealta=new Date()));
            System.out.println("Telefono: " + this.telefono);
            System.out.println("Movil:"+" " + this.movil);
            System.out.println("Historial:"+" " + this.historial);
            System.out.println("Direccion:"+" "+ this.direccion);
        }

    /**
     * agrega los pedidos
     * @param pedido
     * @return pedido=pedido
     */
    public void agregarpedido(Pedido pedido){
            System.out.println("Introduce el pedido:");
            return;
        }
}


