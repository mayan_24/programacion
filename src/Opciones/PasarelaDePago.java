package Opciones;
/**
 * <h1>Pasarela de Pago</h1>
 * <p>
 *     Esta es una parte de la pasarela de pago que contienen
 *     las variables y una opcionciones que no estan diponible ahora
 * </p>
 * @author Angel mayancela saeteros
 * @version 1.0
 * @since 09/02/2022
 */

public class PasarelaDePago {
    //Atributos
    /**
     * El importe del pedido
     * @var double
     */
    private double importe;
    /**
     * El codigo del pago
     * @var int
     */
    private int codigo_pago;


    //constructor vacio
    public PasarelaDePago(){

    }
    /**
     * Función para devolver el importe
     * @param "pide el importe por pantalla"
     */
    public double getImporte() {
        return importe;
    }

    /**
     * Función para introduce el importe
     * @return importe,
     */
    public void setImporte(double importe) {
        this.importe = importe;
    }

    /**
     * Función para devolver el codigo_pago
     * @param "pide el codigo de pago"
     */
    public int getCodigo_pago() {
        return codigo_pago;
    }

    /**
     * Función para introduce el codigo_pago
     * @return codigo_pago,
     */
    public void setCodigo_pago(int codigo_pago) {
        this.codigo_pago = codigo_pago;
    }


    //muestra las variables
    /**
     * muestra la pasarela en el main
     * @param
     * @return importe=importe,codigo_pago=codigo_pago
     */
    public void mostrarpasarela() {
        System.out.println("***Pasarela de Pago***");
        System.out.println();
        System.out.println("Importe:" + " " + this.importe);
        System.out.println("Codigo de Pago:" + " " + this.codigo_pago);
    }
    //Pago en efectivo
    /**
     * La opcion de efectivo
     * @param
     * @return efectivo=efectivo
     */
    public void efectivo(){
        System.out.println("Gracias por pagar en efectivo");
    }
    //Pago en tarjeta
    /**
     * La opcion de tarjeta
     * @param
     * @return tarjeta=tarjeta
     */
    public void tarjeta(){
        System.out.println("Gracias por pagar con tarjeta");
    }
    //Pago en cuenta
    /**
     * La opcion de cuenta
     * @param
     * @return cuenta=cuenta
     */
    public void cuenta(){
        System.out.println("Gracias por pagar con cuenta");
    }


}
