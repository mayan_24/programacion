/**
 * <h1>Pedido</h1>
 * <p>
 *     Esta es una parte del pedido contienen
 *     las variables y una opcionciones que no estan diponible ahora
 * </p>
 * @author Angel mayancela saeteros
 * @version 1.0
 * @since 09/02/2022
 */

package Opciones;

import java.util.Date;
import java.util.Scanner;

public class Pedido {
    //Atributos
    private String cliente;
    /**
     * El nombre del cliente
     * @var string
     */
    private String producto1;
    /**
     * El primer producto
     * @var string
     */
    private String producto2;
    /**
     * El segundo producto
     * @var string
     */
    private int importetotal;
    /**
     * El importe total del pedido
     * @var int
     */
    private String pago;
    /**
     * El pago del cliente
     * @var string
     */
    private String estado;
    /**
     * El estado del cliente
     * @var string
     */

    //constructor vacio
    public Pedido() {

    }
// Constructor
    /**
     * Constructor
     * @param cliente
     * @param producto1
     * @param producto2
     * @param importetotal
     * @param pago
     * @param estado
     */
    public Pedido(String cliente, String producto1, String producto2, int importetotal, String pago, String estado) {
        this.cliente = cliente;
        this.producto1 = producto1;
        this.producto2 = producto2;
        this.importetotal = importetotal;
        this.pago = pago;
        this.estado = estado;
    }

    public String getCliente() {
        return cliente;
    }
    /**
     * Función para devolver el cliente
     * @return cliente,
     */

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }
    /**
     * Función para introduce el cliente
     * @return cliente,
     */

    public String getProducto1() {
        return producto1;
    }
    /**
     * Función para devolver el producto1
     * @return producto1,
     */

    public void setProducto1(String producto1) {
        this.producto1 = producto1;
    }
    /**
     * Función para introduce el producto1
     * @return producto1,
     */

    public String getProducto2() {
        return producto2;
    }
    /**
     * Función para devolver el producto2
     * @return producto2,
     */

    public void setProducto2(String producto2) {
        this.producto2 = producto2;
    }
    /**
     * Función para introduce el producto2
     * @return producto2,
     */

    public int getImportetotal() {
        return importetotal;
    }
    /**
     * Función para devolver el importe total
     * @return importetotal,
     */

    public void setImportetotal(int importetotal) {
        this.importetotal = importetotal;
    }
    /**
     * Función para introduce el importe total
     * @return importetotal,
     */

    public String getPago() {
        return pago;
    }
    /**
     * Función para devolver el pago
     * @return pago,
     */

    public void setPago(String pago) {
        this.pago = pago;
    }
    /**
     * Función para introduce el pago
     * @return pago,
     */

    public String getEstado() {
        return estado;
    }
    /**
     * Función para devolver el estado
     * @return estado,
     */

    public void setEstado(String estado) {
        this.estado = estado;
    }
    /**
     * Función para introduce el estado
     * @return estado,
     */


    /**
     * Agrega el cliente
     * @param cliente1
     * @return cliente=cliente
     */
    public void agregarcliente(Cliente cliente1){
       cliente1 = cliente1;
    }

    /**
     * Agrega el producto
     * @param producto
     * @return protucto=producto
     */

    public void agregarproducto1(Producto producto){
        producto =producto;
    }

    /**
     * Agrega el producto2
     * @param producto2
     * @return protucto2=producto2
     */

    public void agregarproducto2(Producto producto2){
        producto2 =producto2;
    }

    /**
     * Elimina el producto
     * @param producto
     * @return protucto=producto
     *
     */

    public void eliminarproducto1(Producto producto){
        producto = null;
        return;
    }
    /**
     * Elimina el producto
     * @param producto2
     * @return protucto=producto
     *
     */

    public void eliminarproducto2(Producto producto2){
        producto2 = null;
        return;
    }


// ver el pedido en el main
    /**
     * Muetra el pedido en el main
     * @param
     * @return cliente=cliente,producto1=producto1,producto2=procuto2,importetotal=importetotal,pago=pago,estado=estado
     *
     */
    public void verpedido(){
        System.out.println("***Pedido***");
        System.out.println();
        System.out.println("Cliente:" + " " + this.cliente);
        System.out.println("Producto 1:" + " " + this.producto1);
        System.out.println("Producto 2:" + " " + this.producto2);
        System.out.println("Importe Total:" + " " + this.importetotal);
        System.out.println("Pago:" + " " + this.pago);
        System.out.println("Estado:" + " " + this.estado);
    }

}










